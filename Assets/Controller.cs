using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public Camera cam;
    private InputData input;
    private MoveScript charaterMovement;


    // Start is called before the first frame update
    void Start()
    {
        charaterMovement = GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {       
        input.getInput();              

        charaterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash, input.sprint);        
       
    }
}




