using System;
using UnityEngine;

[Serializable]
public struct InputData
{
    public float hMovement;
    public float vMovement;

    public float verticalMouse;
    public float horizontalMouse;

    public bool dash;
    public bool jump;
    public bool sprint;


    public void getInput()
    {
        hMovement = Input.GetAxis("Horizontal");
        vMovement = Input.GetAxis("Vertical");

        verticalMouse = Input.GetAxis("Mouse Y");
        horizontalMouse = Input.GetAxis("Mouse X");

        dash = Input.GetButtonDown("Dash");
        jump = Input.GetButton("Jump");
        sprint = Input.GetButton("Sprint");

    }


}

