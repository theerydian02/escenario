using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CinematicController : MonoBehaviour
{
    public GameObject player;
    public GameObject runner;
    public GameObject runner2;
    public GameObject player2;
    public Text texto;
    public Text textoSombra;

    // Start is called before the first frame update
    void Start()
    {
        if (name.Contains("2"))
        {
            StartCoroutine("Cinematica2");          
        }
        else
        {
            StartCoroutine("Cinematica1");
        }
    }

    // Update is called once per frame
    void Update()
    {
      //  textoSombra.text = texto.text;
    }

    IEnumerator Cinematica1()
    {
        yield return new WaitForSeconds(25f);
        player.SetActive(true);
        Physics.gravity = new Vector3(0,-9.8f,0);
        this.gameObject.SetActive(false);
    }

    IEnumerator Cinematica2()
    {
        yield return new WaitForSeconds(3f);
        texto.text = "Bienvenido a la base Cepheus-5478.";
        yield return new WaitForSeconds(4f);
        texto.text = "Me alegro de que hayas podido llegar sano y salvo...";
        yield return new WaitForSeconds(4f);
        texto.text = "�C�mo?, �Te han atacado?";
        yield return new WaitForSeconds(4f);
        texto.text = "Vaya, lo lamento... �ltimamente hay muchas naves de bandidos por la zona.";
        yield return new WaitForSeconds(5f);
        texto.text = "Pero no te preocupes, en la base hay muchos miembros de la tripulaci�n que podr�n ayudarte.";
        yield return new WaitForSeconds(6f);
        texto.text = "�Qu�? �C�mo que no hay nadie?";
        yield return new WaitForSeconds(4f);
        texto.text = "Mierda... Algo va mal.";
        yield return new WaitForSeconds(5f);
        texto.text = "Muy mal...";
        yield return new WaitForSeconds(7f);
        texto.text = "";
        yield return new WaitForSeconds(9f);
        texto.text = "�Corre! Este lugar no es seguro...";
        yield return new WaitForSeconds(3f);
        texto.text = "Por la izquierda encontrar�s el ala m�dica, si llegas hasta all� podr�s esconderte y curar tus heridas";
        yield return new WaitForSeconds(2f);
        player.SetActive(false);
        yield return new WaitForSeconds(5f);
        texto.text = "Date pri -Conexi�n terminada-";
        runner.SetActive(true);
        yield return new WaitForSeconds(3f);
        texto.text = "";
        runner2.SetActive(true);
        yield return new WaitForSeconds(31f);
        player2.SetActive(true);
        this.gameObject.SetActive(false);
    }

}

