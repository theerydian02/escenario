using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCine : MonoBehaviour
{
    public GameObject text;
    public GameObject director;
    public GameObject playercam;
    public GameObject player;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            text.SetActive(true);
        }
    }
        private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {           
            if (Input.GetKeyDown(KeyCode.F))
            {
                director.SetActive(true);
                playercam.SetActive(false);               
                text.SetActive(false);
                player.GetComponent<MoveScript>().Speed = 0;
                player.GetComponent<MoveScript>().StopRandomIdle = true;
                player.GetComponent<Controller>().enabled = false;
            //    player.GetComponent<Animator>().enabled = false;
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        text.SetActive(false);
    }

}
