using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizarLlave : MonoBehaviour
{
    public GameObject Foto;
    public GameObject texto;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            texto.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Foto.SetActive(true);
                texto.SetActive(false);              
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        texto.SetActive(false);
    }
}
