using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activarPortal : MonoBehaviour
{
    public GameObject Portal;
    public GameObject texto;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            texto.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Portal.SetActive(true);
                texto.SetActive(false);
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        texto.SetActive(false);
    }
}
