using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{
    public bool StopRandomIdle;

    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    [Range(0, 180f)]
    public float degreesToTurn = 160f;

    [Header("Animation Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private float wallRay;
    public float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    public bool turn180;

    bool timer = false;
    public float floorDist;
    public float floorDist3;
    public float floorDist2;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            floorDist = hit.distance;
            Debug.DrawRay(transform.position, -Vector3.up, Color.green);
        }
       
        if (Physics.Raycast(new Vector3(transform.position.x - 0.6f, transform.position.y+ 0.6f, transform.position.z - 0.6f), -Vector3.up, out hit))
        {
            floorDist2 = hit.distance;
            Debug.DrawRay(new Vector3(transform.position.x - 0.6f, transform.position.y + 0.6f, transform.position.z - 0.6f), -Vector3.up, Color.green);
        }
        
        if (Physics.Raycast(new Vector3(transform.position.x + 0.6f, transform.position.y + 0.6f, transform.position.z + 0.6f), -Vector3.up, out hit))
        {
            floorDist3 = hit.distance;
            Debug.DrawRay(new Vector3(transform.position.x + 0.6f, transform.position.y + 0.6f, transform.position.z + 0.6f), -Vector3.up, Color.green);
        }


        if (floorDist < 10 || floorDist2 < 10 || floorDist3 < 10) 
        {
            animator.SetBool("landed", true);
        }
        else
        {
            animator.SetBool("landed", false);
        }
                
      
        
    }


    

    // Update is called once per frame
    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash, bool sprint)
    {
        if (dash)
        {
            animator.SetBool("dash", true);
        }
        else
        {
            animator.SetBool("dash", false);
        }   
    

        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if (sprint)
        {            
            animator.SetBool("sprint", true);
        }
        else
        {
            animator.SetBool("sprint", false);
        }

        if (Speed>=Speed-rotationThreshold && dash)
        {
            Speed = 1.5f;
        }


        if (jump)
        {
            animator.SetBool("jump", true);
        }
        else
        {
            animator.SetBool("jump", false);
        }
    

        if (Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;

            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                animator.SetBool(turn180Param, true);
            }
            else
            {
                animator.SetBool(turn180Param, false);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
            }
        }
        else if (Speed < rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }


        if (hInput==0 && vInput == 0 && jump == false && dash == false )
        {
            if (timer == false)
            {
                StartCoroutine("idleRandom");
                timer = true;
            }
        }
        else
        {
            StopCoroutine("idleRandom");
            animator.SetInteger("Idle", 0);

           timer = false;
        }       
    }

    IEnumerator idleRandom()
    {      
       
        yield return new WaitForSeconds(Random.Range(20,40));
        if (StopRandomIdle == false)
            animator.SetInteger("Idle", Random.Range(1,3));      
    }



}
