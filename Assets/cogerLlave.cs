using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cogerLlave : MonoBehaviour
{
    public GameObject llave;
    public GameObject texto;
    public GameObject puerta;
    public GameObject trig;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            texto.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                llave.SetActive(false);
                texto.SetActive(false);
                puerta.SetActive(false);
                trig.SetActive(false);
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        texto.SetActive(false);
    }
}
