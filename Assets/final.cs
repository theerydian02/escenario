using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class final : MonoBehaviour
{
    public GameObject textFin;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            textFin.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            textFin.SetActive(false);
        }
    }
}
